#!/bin/bash
# Acept a directory with csvs to parse

#DEBUG='echo'
TARIFICADOR="/home/the00z/neuroservices/tarificador/put"
DIR="$1"

find "$DIR" -type d | grep -v -G "^$DIR$" | sort | while read SUBDIR
do
  TMP=$( find "$SUBDIR" -iname '*csv*')

# if there are no modified csvs then use the normal csv.

  if ! CSV=$( grep -i 'mod' <<<"$TMP");then
    if ! CSV=$(grep -i 'master.csv' <<<"$TMP"); then
      continue
    fi
  fi

  CSV=$(sed -ne '/\(csv\|mod\)$/p'<<<"$CSV")
  EDIFICIO="'$(sed -e 's/^\(.*\/\)\?\([[:digit:]]\+\)[[:space:]].*$/\2/g' \
      <<<"$CSV")'"
  echo "$TARIFICADOR" "'$CSV'" "'$EDIFICIO'" >/dev/stderr
  eval "$TARIFICADOR" "'$CSV'" "'$EDIFICIO'" 

  echo '###############################################'
done
