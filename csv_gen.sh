#!/bin/bash
# An script to generate a csv.
DEBUG='echo'
GET="/home/the00z/neuroservices/tarificador/get"
TMP=`getopt -o h --long help -- "$@"`

eval set -- "$TMP"

while true
do
  case "$1" in
  h|help)
    echo "Ayuda."
    exit 1
    ;;
    --)
    shift
    break
    ;;
  esac
done

if [ -z "$@" ]
then 
  echo "I need an argument at least to work"
fi

for EDIFICIO in  $@
do
   
eval 'time' "'$GET'"  > "$EDIFICIO.csv" <<END
  select  
    date(llamada.start), time(llamada.start),'','1.49', llamada.destino,
    duration,concat('0',edificio_extension.edificio_id,extension.numero),'',
    llamada.cuenta, '' 
  from 
    edificio, edificio_extension, extension , extension_llamada , llamada 
  where 
    edificio.id='${EDIFICIO}' and
    edificio.id<=>edificio_extension.edificio_id and
    edificio_extension.extension_id<=>extension.id and
    extension.id<=>extension_llamada.extension_id and
    extension_llamada.llamada_id<=>llamada.id
    and llamada.d_context regexp '^(cs-|in|celu)' 
    and llamada.cuenta != ''
    and llamada.disposition regexp '^answered' 
    and llamada.last_app regexp 'dial' 
    and llamada.d_context not regexp 'interno' 
    and llamada.bill_sec !=  '0' 
    and llamada.destino regexp '^9'
    and extension.numero !=''
  order 
    by llamada.start
END

     if [ "$?" != "0" ]; then exit "$?";fi
done
